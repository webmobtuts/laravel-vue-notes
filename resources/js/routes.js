import AddNote from './components/notes/AddNote';
import EditNote from './components/notes/EditNote';
import ListNotes from './components/notes/ListNotes';
import Login from './components/auth/Login';
import Register from './components/auth/Register';

const routes = [
    { path: '/', component: ListNotes, meta: { auth: true } },
    { path: '/add', component: AddNote, meta: { auth: true } },
    { path: '/edit/:id', component: EditNote, meta: { auth: true } },
    { path: '/login', component: Login, meta: { auth: false } },
    { path: '/register', component: Register, meta: { auth: false } }
];

export default routes;