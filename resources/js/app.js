require('./bootstrap');
import Vue from 'vue';
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueRouter from 'vue-router';
import routes from './routes';
import App from './components/App';
import CKEditor from '@ckeditor/ckeditor5-vue';

Vue.use(VueAxios, axios);

Vue.use(VueRouter);

Vue.use( CKEditor );

axios.defaults.baseURL = window.base_url + "/api";

const router = new VueRouter({
    routes
});

Vue.router = router;

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x'),
});

App.router = Vue.router;

new Vue(App).$mount('#app');
