<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteValidator;
use App\Note;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class NotesController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Note::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->paginate(10);

        return response()->json(['data' => $notes], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NoteValidator $request)
    {
        $request->validated();

        $note = new Note();

        $note->title = $request->input('title');

        $note->description = $request->input('description');

        $note->user_id = Auth::user()->id;

        $note->save();

        return response()->json(['message' => 'Created successfully', 'data' => $note], Response::HTTP_CREATED);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $note = Note::findOrFail($id);

        if(!$note) {
            return response()->json(['message' => 'note not found'], Response::HTTP_NOT_FOUND);
        }

        return response()->json(['data' => $note], Response::HTTP_OK);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NoteValidator $request, $id)
    {
        $note = Note::findOrFail($id);

        if(!$note) {
            return response()->json(['message' => 'note not found'], Response::HTTP_NOT_FOUND);
        }

        $request->validated();

        $note->title = $request->input('title');

        $note->description = $request->input('description');

        $note->save();

        return response()->json(['message' => 'Updated successfully', 'data' => $note], Response::HTTP_OK);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Note::findOrFail($id);

        if(!$note) {
            return response()->json(['message' => 'note not found'], Response::HTTP_NOT_FOUND);
        }

        $note->delete();

        return response()->json(['message' => 'Deleted successfully'], Response::HTTP_OK);
    }
}
