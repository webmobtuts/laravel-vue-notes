<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class NotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,30) as $index) {
            \Illuminate\Support\Facades\DB::table('notes')->insert([
                'title' => $faker->title,
                'description' => $faker->paragraph
            ]);
        }
    }
}
